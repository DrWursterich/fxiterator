# Changelog
All notable changes to this project have been documented in this file.

The format is loosely based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), 
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2020-05-29
- Extended Samples
- Added Limits for Items held
- Added Default Buttons for IteratorItemWrappers
- Adjusted the Package-Structure and Naming
- Added HIterator
- Added Possibility to sync with external Lists
- Added getItems Method for Iterators

