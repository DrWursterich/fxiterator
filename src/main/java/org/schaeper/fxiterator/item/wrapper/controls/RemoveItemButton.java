package org.schaeper.fxiterator.item.wrapper.controls;

import org.schaeper.fxiterator.Iterator;
import org.schaeper.fxiterator.indexer.Indexer;
import org.schaeper.fxiterator.item.Item;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.Button;

public class RemoveItemButton extends Button {
	private ObjectProperty<Indexer> indexerProperty;
	private static final String DEFAULT_TEXT = "X";

	public <T extends Item> RemoveItemButton(
			final Iterator<T> iterator, 
			final T item,
			final Indexer indexer) {
		this.indexerProperty = new SimpleObjectProperty<Indexer>(indexer);
		this.setText(RemoveItemButton.DEFAULT_TEXT);
		this.setOnAction(e -> {
			final int index = this.indexerProperty.get().getIndexOf(
					iterator,
					item);
			if (index >= 0) {
				iterator.removeElement(index);
			}
		});
		this.disableProperty().bind(
				iterator.sizeProperty()
					.lessThanOrEqualTo(iterator.getMinElements()));
	}

	public Indexer getIndexer() {
		return this.indexerProperty.get();
	}

	public void setIndexer(final Indexer indexer) {
		this.indexerProperty.set(indexer);
	}
	
	public ObjectProperty<Indexer> indexerProperty() {
		return this.indexerProperty;
	}
}
