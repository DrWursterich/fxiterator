package org.schaeper.fxiterator.item.wrapper;

import org.schaeper.fxiterator.Iterator;
import org.schaeper.fxiterator.item.Item;

import javafx.scene.layout.Pane;

@FunctionalInterface
public interface WrapperFactory<T extends Item> {

	public abstract Pane createWrapper(Iterator<T> iterator, T item);
}
