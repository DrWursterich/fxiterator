package org.schaeper.fxiterator.item.wrapper.controls;

import org.schaeper.fxiterator.Iterator;
import org.schaeper.fxiterator.indexer.Indexer;
import org.schaeper.fxiterator.item.Item;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.Button;

public class AddItemButton extends Button {
	private ObjectProperty<Indexer> indexerProperty;
	private static final String DEFAULT_TEXT = "+";

	public <T extends Item> AddItemButton(
			final Iterator<T> iterator, 
			final T item,
			final Indexer indexer) {
		this.indexerProperty = new SimpleObjectProperty<Indexer>(indexer);
		this.setText(AddItemButton.DEFAULT_TEXT);
		this.setOnAction(e -> {
			final int index = this.indexerProperty.get().getIndexOf(
					iterator,
					item);
			if (index >= 0) {
				iterator.insertElement(index);
			}
		});
		this.disableProperty().bind(
				iterator.sizeProperty()
					.greaterThanOrEqualTo(iterator.getMaxElements()));
	}

	public Indexer getIndexer() {
		return this.indexerProperty.get();
	}

	public void setIndexer(final Indexer indexer) {
		this.indexerProperty.set(indexer);
	}
	
	public ObjectProperty<Indexer> indexerProperty() {
		return this.indexerProperty;
	}
}
