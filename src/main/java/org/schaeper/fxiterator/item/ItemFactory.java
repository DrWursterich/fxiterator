package org.schaeper.fxiterator.item;

@FunctionalInterface
public interface ItemFactory<T extends Item> {
	public abstract T createItem();
}
