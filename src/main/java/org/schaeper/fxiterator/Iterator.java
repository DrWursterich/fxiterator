package org.schaeper.fxiterator;

import java.util.List;
import java.util.function.BiConsumer;

import org.schaeper.fxiterator.item.Item;

import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.ReadOnlyListProperty;

public interface Iterator<T extends Item>  {

	public void insertElement(int index);

	public void removeElement(int index);

	public void removeElement(T object);

	public int indexOf(T object);

	public <E> void applyList(
			final List<E> list,
			final BiConsumer<E, T> mappingFunction);

	public List<T> getItems();

	public ReadOnlyListProperty<T> itemsProperty();

	public int size();

	public ReadOnlyIntegerProperty sizeProperty();

	public int getMaxElements();

	public int getMinElements();
}
