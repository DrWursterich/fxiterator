package org.schaeper.fxiterator.indexer;

import org.schaeper.fxiterator.Iterator;
import org.schaeper.fxiterator.item.Item;

public class ItemPositionIndexer extends Indexer {

	public ItemPositionIndexer(final int offset) {
		super(offset);
	}

	public ItemPositionIndexer() {
		this(0);
	}

	@Override
	public <T extends Item> int calculateIndex(
			final Iterator<T> iterator, 
			final T item) {
		return iterator.indexOf(item);
	}
}
