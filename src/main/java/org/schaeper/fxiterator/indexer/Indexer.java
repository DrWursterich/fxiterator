package org.schaeper.fxiterator.indexer;

import org.schaeper.fxiterator.Iterator;
import org.schaeper.fxiterator.item.Item;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

public abstract class Indexer {
	private final IntegerProperty offsetProperty;

	public Indexer(final int offset) {
		this.offsetProperty = new SimpleIntegerProperty(offset);
	}

	public Indexer() {
		this(0);
	}

	/**
	 * Calculates an Index from an {@link Iterator Iterator} and 
	 * an {@link Item IteratorItem}.<br/>
	 * Convention is, to return <b>-1</b> if no Index is calculatable.
	 * @param <T> The Type of IteratorItem
	 * @param iterator The Iterator
	 * @param item The IteratorItem
	 * @return The Index
	 */
	protected abstract <T extends Item> int calculateIndex(
			final Iterator<T> iterator, 
			final T item);
	
	public <T extends Item> int getIndexOf(
			final Iterator<T> iterator, 
			final T item) {
		final int index = this.calculateIndex(iterator, item);
		return index >= 0 ? (index + this.offsetProperty.get()) : index;
	}
	
	public int getOffset() {
		return this.offsetProperty.get();
	}
	
	public void setOffset(final int offset) {
		this.offsetProperty.set(offset);
	}
	
	public IntegerProperty offsetProperty() {
		return this.offsetProperty;
	}
}
