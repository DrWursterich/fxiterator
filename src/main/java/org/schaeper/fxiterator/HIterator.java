package org.schaeper.fxiterator;

import java.util.Collections;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.stream.IntStream;

import org.schaeper.fxiterator.item.Item;
import org.schaeper.fxiterator.item.ItemFactory;
import org.schaeper.fxiterator.item.wrapper.WrapperFactory;

import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.ReadOnlyListProperty;
import javafx.beans.property.ReadOnlyListWrapper;
import javafx.collections.FXCollections;
import javafx.scene.Node;
import javafx.scene.layout.HBox;

public class HIterator<T extends Node & Item> extends HBox
		implements Iterator<T> {
	private final ReadOnlyListWrapper<T> itemsProperty;
	private final int minElements;
	private final int maxElements;
	private ItemFactory<T> itemFactory;
	private WrapperFactory<T> wrapperFactory;

	public HIterator(
			final ItemFactory<T> itemFactory,
			final WrapperFactory<T> wrapperFactory,
			final int minElements,
			final int maxElements) {
		this.itemsProperty = new ReadOnlyListWrapper<>(
				FXCollections.observableArrayList());
		this.itemFactory = itemFactory;
		this.wrapperFactory = wrapperFactory;
		this.minElements = minElements;
		this.maxElements = maxElements;
		IntStream.range(0, minElements).forEachOrdered(this::insertElement);
	}

	public HIterator(
			final ItemFactory<T> itemFactory,
			final WrapperFactory<T> wrapperFactory) {
		this(itemFactory, wrapperFactory, 1, Integer.MAX_VALUE);
	}

	@Override
	public void insertElement(final int index)
			throws IndexOutOfBoundsException, UnsupportedOperationException {
		if (this.itemsProperty.size() >= this.maxElements) {
			return;
		}
		final T item = this.itemFactory.createItem();
		this.itemsProperty.add(index, item);
		this.getChildren().add(
				index,
				this.wrapperFactory.createWrapper(this, item));
	}

	@Override
	public void removeElement(final int index)
			throws IndexOutOfBoundsException, UnsupportedOperationException {
		if (this.itemsProperty.size() <= this.minElements) {
			return;
		}
		this.itemsProperty.remove(index);
		this.getChildren().remove(index);
	}

	@Override
	public void removeElement(T object)
			throws ClassCastException,
				NullPointerException,
				UnsupportedOperationException {
		final int index = this.itemsProperty.indexOf(object);
		if (index != -1) {
			this.itemsProperty.remove(index);
			this.getChildren().remove(index);
		}
	}

	@Override
	public <E> void applyList(
			final List<E> list,
			final BiConsumer<E, T> mappingFunction) {
		final List<T> items = this.itemsProperty.get();
		final int desiredItemAmount = Math.max(this.minElements, list.size());
		if (items.size() < desiredItemAmount) {
			IntStream.range(items.size(), desiredItemAmount)
					.forEachOrdered(this::insertElement);
		}
		if (items.size() > desiredItemAmount) {
			IntStream.iterate(items.size() - 1, i -> i - 1)
					.limit(items.size() - desiredItemAmount)
					.forEachOrdered(this::removeElement);
		}
		IntStream.range(0, list.size())
				.forEachOrdered(i -> {
					mappingFunction.accept(list.get(i), items.get(i));
				});
	}

	@Override
	public int indexOf(final T object) {
		return this.itemsProperty.indexOf(object);
	}

	@Override
	public ReadOnlyListProperty<T> itemsProperty() {
		return this.itemsProperty.getReadOnlyProperty();
	}

	@Override
	public List<T> getItems() {
		return Collections.unmodifiableList(this.itemsProperty.get());
	}

	@Override
	public int size() {
		return this.itemsProperty.size();
	}

	@Override
	public ReadOnlyIntegerProperty sizeProperty() {
		return this.itemsProperty.sizeProperty();
	}

	@Override
	public int getMaxElements() {
		return this.maxElements;
	}

	@Override
	public int getMinElements() {
		return this.minElements;
	}
}
