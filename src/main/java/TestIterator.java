import org.schaeper.fxiterator.Iterator;
import org.schaeper.fxiterator.VIterator;
import org.schaeper.fxiterator.item.Item;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

public class TestIterator extends VIterator<TestIterator.TestItem> {

	static class TestItem extends HBox implements Item {
		public TestItem() {
			final Label label = new Label("ICH BIN DAS ITEM");
			this.getChildren().addAll(label);
		}
	}

	public TestIterator() {
		super(TestItem::new, TestIterator::createItemWrapper);
	}

	private static Pane createItemWrapper(
			final Iterator<TestItem> iterator,
			final TestItem item) {
		final GridPane grid = new GridPane();
		final Button remove = new Button("X");
		remove.setOnAction(e -> {
			iterator.removeElement(item);
		});
		final Button addBefore = new Button("+");
		addBefore.setOnAction(e -> {
			final int index = iterator.indexOf(item);
			if (index == -1) {
				return;
			}
			iterator.insertElement(index);
		});
		final Button addAfter = new Button("+");
		addAfter.setOnAction(e -> {
			final int index = iterator.indexOf(item);
			if (index == -1) {
				return;
			}
			iterator.insertElement(index + 1);
		});
		grid.add(addBefore, 1, 0);
		grid.add(item, 0, 1);
		grid.add(remove, 1, 1);
		grid.add(addAfter, 1, 2);
		return grid;
	}
}
