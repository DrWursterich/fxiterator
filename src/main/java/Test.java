import org.schaeper.fxiterator.Iterator;
import org.schaeper.fxiterator.VIterator;
import org.schaeper.fxiterator.indexer.Indexer;
import org.schaeper.fxiterator.indexer.ItemPositionIndexer;
import org.schaeper.fxiterator.item.Item;
import org.schaeper.fxiterator.item.wrapper.controls.AddItemButton;
import org.schaeper.fxiterator.item.wrapper.controls.RemoveItemButton;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class Test extends Application {

	public static void main(String...args) {
		Application.launch();
	}

	static class TestItem extends HBox implements Item {
		public TestItem() {
			final Label label = new Label("ICH BIN DAS ITEM");
			this.getChildren().addAll(label);
		}
	}

	@Override
	public void start(final Stage stage) throws Exception {
		stage.setScene(
				new Scene(
					new HBox(
						new TestIterator(),
						new VIterator<>(
							TestItem::new,
							Test::createVWrapper,
							1,		//minItems
							4))));	//maxItems
		stage.show();
	}

	public static Pane createVWrapper(
			final Iterator<TestItem> iterator,
			final TestItem item) {
		final GridPane grid = new GridPane();
		final Indexer iteratorIndexer = new ItemPositionIndexer();
		final Button remove = new RemoveItemButton(
				iterator, 
				item, 
				iteratorIndexer);
		final Button addBefore = new AddItemButton(
				iterator, 
				item, 
				iteratorIndexer);
		final Button addAfter = new AddItemButton(
				iterator, 
				item, 
				new ItemPositionIndexer(1));
		grid.add(addBefore, 1, 0);
		grid.add(item, 0, 1);
		grid.add(remove, 1, 1);
		grid.add(addAfter, 1, 2);
		return grid;
	}
}
